# This script is used to solve the class exercises
#Exercise #1

ticker_symbol = 'TSLA'
ticker_price = 321.64
trading_amount = 1000
trading_action = 'buy'
print( 'I would like to', trading_action, trading_amount, 'shares in', ticker_symbol)

#Exercise #2
person_1 = 'Wilson'
borrow_amount = 10000
person_2 = 'Snow'
stock_amount = 50
stock = 'TSLA'

print(person_1, 'needs to borrow', borrow_amount, 'euros from',  person_2,
      'in order to trade', stock_amount, 'stocks in company', stock)

#Exercise #3
var_1 = 6
var_2 = 7
result = var_1 * var_2

#Exercise #4

celsius = 10
fahrenheit = celsius * (9/5) + 32
kelvin = celsius + 273.15

print(celsius, 'celsius is', fahrenheit, 'fahrenheit and', kelvin, 'kelvin')

print('hello world')

