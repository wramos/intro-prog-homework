# This script is used to solve the class exercises

#Exercise #1

def power(a, b):
    return a ** b

#Exercise #2
def find_if_equal(a, b):
    return a == b

#Exercise #3
def calc_hypotenuse(side1, side2):
    return (side1 ** 2 + side2 ** 2) ** (1/2)

#Exercise #4
def convert_BTC_to_LTC(bitcoin_value):
    return bitcoin_value * 54.31

def convert_BTC_to_ETH(bitcoin_value):
    return bitcoin_value * 10.224998

def convert_BTC_to_EUR(bitcoin_value):
    return bitcoin_value * 6971.54

#Exercise #5
"""
When a function doesn't have a return statement no output is returned, although
the code inside the function runs as normal.
"""   
def no_return(a,b):
    print(a,b)
    return

a = no_return(5,6)
# Returns <class 'NoneType'>

#Exercise #6
"""
A function that prints a value only displays the value in the console REPL.
If you try to save the result of the function, the result variable will be empty.
A function that returns a values, you can store it in a variable for later use.
"""
#Exercise #7
