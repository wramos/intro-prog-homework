# Exercise #1
"""
# Way 1
from feline import cat

cat.speak()

# Way 2
import feline.cat

feline.cat.speak()

# Way 3
from feline.cat import speak

speak()
"""
# Exercise #2
"""

from canine import dog
"""

