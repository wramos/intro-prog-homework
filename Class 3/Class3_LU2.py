# This script is used to solve the class exercises
#Exercise #0
"""
The index of 'c' is 2
The index of 'a' is 0
The index of 'b' is 1
"""
my_tuple = ('a', 'b', 'c')
print(my_tuple[0], my_tuple[1], my_tuple[2])

#Exercise #1
# An error occurs because a tuple can never be changed

#Exercise #2
apple_jan = (100, 101, 102, 103, 104)

#Exercise #3
# An error occurs because a tuple can never be changed

#Exercise #4

apple_jan_list = [100, 101, 102, 103, 104, 105]
apple_jan_list.append(106)
print(apple_jan_list)

#Exercise #5
"""
Both of them are data structures to each variables can be assigned to. The
difference is that tuples cannot be changes while lists can be.
"""

#Exercise #6
"""
Equal
Not Equal
Equal
Tuples and lists are considered equal when their length is the same and every
entry is equal to one another.
"""
#Exercise #7
my_tuple2 = (1, 2, 3)
tuple_into_list = list(my_tuple2)
print(tuple_into_list)

#Exercise #8
# Example dict
my_dict = {'a': 1, 'b': 2}
"""
keys() - returns every key present in the dictionary
print(type(my_dict.keys())) #Type is dict_keys
values() - returns every value present in the dictionary
print(type(my_dict.values())) #Type is dict_values
items() - returns every key, value combination present in the dictionary
print(type(my_dict.items())) #Type is dict_items
"""

#Exercise #9

my_dict2 = {}
my_dict2['hello'] = 'world'
my_dict2['goodbye'] = 'world'
print(my_dict2)

#Exercise #10
"""
stock_prices['APPL'] is returned which is 100
"""

#Exercise #11

# Dictionary normal way
dict_normal = {'name': 'Wilson', 'age': 23}
# Dictionary from a list of tuples
dict_not_normal = dict([('name', 'Wilson'), ('age', 23)])
print(dict_normal, dict_not_normal)

#List normal away
list_normal = ['dog', 'cat', 'fish']
# List from tuple
list_not_normal = list(('dog', 'cat', 'fish'))
print(list_normal, list_not_normal)

#tuple normal away
tuple_normal = ('dog', 'cat', 'fish')
# Tuple from list
tuple_not_normal = tuple(['dog', 'cat', 'fish'])
print(tuple_normal, tuple_not_normal)

#Exercise #12
my_dict = {'a': 1, 'b': 2}
# One way
my_dict.pop('a')
print(my_dict)

my_dict = {'a': 1, 'b': 2}
# Another way
del my_dict['b']
print(my_dict)

# In the first way the value of the 'a' key can be stored for later use
# In the second way is not possible to store the result

#Exercise #13
def add_homie(initial_list):
    initial_list.append('homie')
    return initial_list

list1 = ['a', 'car', 'bike']
end_list = add_homie(list1)
print(end_list)

"""
Attention, this would not work:
def add_homie(initial_list):
    return initial_list.append('homie')

I think because the append function is already assigning it's result to 
initial_list and it's not possible to assign it at the same time to other
variable, so this is wrong: (example)
end_list = initial_list.append('homie')
"""

#Exercise #14
GOOG = 788
APPL = 3783
KPMG = 31
BCG = 1232
MSFT = 34
OSYS = 23
PHZR = 3

def sum_stocks(*stock):
    # This saves every argument in a tuple. In this case:
    # stock = (788, 3783, ..., 3)
    #See: http://introtopython.org/more_functions.html#Accepting-an-arbitrary-number-of-arguments
    return sum(stock)/len(stock)

print(sum_stocks(GOOG, APPL, KPMG, BCG, MSFT, OSYS, PHZR))

# With only one argument
def sum_stocks2(stock):
    return sum(stock)/len(stock)

print(sum_stocks2([GOOG, APPL, KPMG, BCG, MSFT, OSYS, PHZR]))











